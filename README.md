# Tech Test
Laravel Contact Form. This application follows all the guidelines sent to me. These are:

- Allow the user to fill in their name, email, subject and message.
- The application must validate the input as follows:
    - Name => Required,
    - Email => Required and Valid Email
    - Subject => Required
    - Message => Required
- The submitted information should be stored in the Database
- The system must send two emails one to the website owner with the content submitted and one to the user letting them know that their message has been received.
- Display all submitted messages in a table accessible via the /messages url.

I am using MailTrap.io to send emails, it is a demo application that does not send live emails, but it shows the email as sent in the application.

## Installation instructions

- Pull this repo and fill in your details in your .env file (you can do cp `.env.example .env` in terminal if you wish)
- Assuming you have a mailtrap.io account, fill in your inbox details in .env
- Type `php artisan key:generate` to generate the application key.
- Migrate the database (I have not added any seeds as of yet)
- Run `php artisan serve` and contact away

Please let me know of any issues!