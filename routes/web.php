<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContactController@create');
Route::post('/submit', 'ContactController@store');

Route::get('/messages', 'ContactController@index');
Route::get('/messages/view/{id}', 'ContactController@show');