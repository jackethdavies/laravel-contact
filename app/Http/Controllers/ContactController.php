<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Mail;

class ContactController extends Controller
{



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $findMessages = Contact::orderBy('created_at', 'desc')->paginate(10);
        return view('messages')->with([
            "messages" => $findMessages
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|max:255',
            'subject' => 'required|max:255',
            'email' => 'required|max:255|email',
            'message' => 'required|max:255'
        ]);
        
        $contact = tap(new Contact($data))->save();
        // Send email to customer
        Mail::to($request->email)->send(new \App\Mail\SendMail($data));

        // Send email to website owner
        Mail::to(config('mail.from.address'))->send(new \App\Mail\SendOwnerMail($data));

        return redirect('/')->with('msg', 'Contact form submitted! Please check your emails.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $findMessage = Contact::findOrFail($id);
        return view('view')->with([
            "message" => $findMessage
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
