@extends('email.layout')

@section('content')
    <h1>CONTACT FORM: {{ $data['subject'] }}</h1>
    <p>{{ $data['message'] }}</p>

    <p>From: {{ $data['name'] }} at {{ $data['email'] }}</p>
@endsection