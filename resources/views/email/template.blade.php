@extends('email.layout')

@section('content')
    <h1>We have recieved your message!</h1>

    <p>Hello, {{ $data['name'] }} <br>
    We have recieved your message, please be patient whilst we get back to you.</p>
@endsection