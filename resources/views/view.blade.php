@extends('layout.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $message->subject }} from <strong><a href="mailto:{{ $message->email }}">{{ $message->name }}</a></strong></div>

                <div class="card-body">
                    {{ $message->message }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection