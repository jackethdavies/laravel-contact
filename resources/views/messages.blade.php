@extends('layout.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">View All Messages</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Subject</th>
                                <th scope="col">Email</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                    <tbody>
                    @foreach($messages as $message)
                        <tr>
                            <td>{{ str_limit($message->name, 20, '...') }}</td>
                            <td>{{ str_limit($message->subject, 20, '...') }}</td>
                            <td>{{ str_limit($message->email, 20, '...') }}</td>
                            <td>{{ $message->created_at }}</td>
                            <td><a href="/messages/view/{{ $message->id }}">View</a></td>                            
                        </tr>                
                    @endforeach

                    {{ $messages->links() }}
                    </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection